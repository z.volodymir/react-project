import React from "react";
import ReactDOM from "react-dom";

class WhoAmI extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			years: 30
		}
		// this.nextYears = () => {
		// 	this.setState(state => ({
		// 		years: ++state.years
		// 	}));
		// }
		this.nextYears = this.nextYears.bind(this);
	}
	nextYears () {
		this.setState(state => ({
			years: ++state.years
		}));
	}

	render() {
		const {name, surname, link} = this.props;
		const {years} = this.state;
		return (
			<>
				<button onClick={this.nextYears} className="btn btn-outline-info">++</button>
				<h1>My name is {name}, my surname is {surname}, years {years}</h1>
				<a href={link}>My profile</a>
			</>
		)
	}
}

const All = () => {
	return (
		<>
			<WhoAmI name="John" surname="Smith" link="#"/>
			<WhoAmI name="Alex" surname="Bolduin" link="#"/>
			<WhoAmI name="Jessica" surname="Part" link="#"/>
		</>
	)
}

ReactDOM.render(<All/>, document.getElementById('root'));