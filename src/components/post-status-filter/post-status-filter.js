import React, {Component} from "react";
import { Button } from "reactstrap";

import "./post-status-filter.css";

export default class PostStatusFilter extends Component{
	constructor(props) {
		super(props);
		this.button = [
			{name: 'all', label: 'Все'},
			{name: 'like', label: 'Понравилось'}
		]
	}

	render() {
		const button = this.button.map(({name, label}) => {
			const {filter, onFilterSelect} = this.props;
			const active = filter === name;
			const activeClass = active ? 'btn-info' : 'btn-outline-secondary';
			return(
				<button
					key={name}
					type="button"
					className={`btn ${activeClass}`}
					onClick={() => onFilterSelect(name)}
				>{label}
				</button>
			)
		});

		return (
			<div className="btn-group">
				{/*<Button outline color="info">Все</Button>*/}
				{button}
			</div>
		)
	}
}
