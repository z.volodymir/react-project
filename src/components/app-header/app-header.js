import React from "react";

import "./app-header.css";
import styled from 'styled-components';

const Header = styled.a`
	display: flex;
	align-items: flex-end;
	justify-content: space-between;
	h1 {
		font-size: 26px;
		cursor: pointer;
		transition: all .2s linear;
		color: ${props => props.colored ? 'black' : 'red'};
		:hover {
			color: #3b539c;
		}
	}
	h2{
		font-size: 1.2rem;
		color: grey;
	}
`

const AppHeader = ({allItem, allLiked}) => {
	return (
		<Header as="div" colored>
			<h1>Name Surname</h1>
			<h2>{allItem} записей, из них понравилось {allLiked}</h2>
		</Header>
	)
}

export default AppHeader;