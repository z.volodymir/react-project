import React from "react";

import AppHeader from "../app-header/app-header";
import SearchPanel from "../search-panel/search-panel";
import PostStatusFilter from "../post-status-filter/post-status-filter";
import PostList from "../post-list/post-list";
import PostAddForm from "../post-add-form/post-add-form";

import "./app.css";
import styled from 'styled-components';

const AppBlock = styled.div`
		margin: 0 auto;
		max-width: 800px;
`;
// const styledAppBlock = styled(AppBlock)`
// 	background-color: gray;
// `

export default class App extends React.Component{
	constructor(props) {
		super(props);
		this.state = {
			data: [
				{label: 'Going to learn React', important: true, like: true, id: 1},
				{label: 'That is so good', important: false, like: false, id: 2},
				{label: 'I need a break...', important: false, like: false, id: 3}
			],
			search: '',
			filter: 'all'
		};

		this.deleteItem = this.deleteItem.bind(this);
		this.addItem = this.addItem.bind(this);
		this.createId = this.createId.bind(this);
		this.onToggleImportant = this.onToggleImportant.bind(this);
		this.onToggleLiked = this.onToggleLiked.bind(this);
		this.searchPosts = this.searchPosts.bind(this);
		this.updateSearch = this.updateSearch.bind(this);
		this.filterPosts = this.filterPosts.bind(this);
		this.onFilterSelect = this.onFilterSelect.bind(this);

		this.newId = 4;
	}

	createId() {
		const newId = Math.random().toString(36).substr(2, 9);
		console.log(newId);

		this.setState(({data}) => {
			data.forEach(arr => {
				if (newId === arr.id) {
					this.createId();
				}
			});
		});
		return newId;
	}

	addItem(body) {
		const newItem = {
			label: body,
			important: false,
			id: this.createId()
		};
		this.setState(({data}) => {
			const newArr = [...data, newItem];
			return {
				data: newArr
			}
		});
	}

	deleteItem(id) {
		this.setState(({data}) => {
			const index = data.findIndex(elem => elem.id === id);

			const newArr = [...data.slice(0, index), ...data.slice(index + 1)];

			return {
				data: newArr
			}
		});
	}

	onToggleImportant(id) {
		this.setState(({data}) => {
			const index = data.findIndex(element => element.id === id);
			const oldItem = data[index];
			const newItem = {...oldItem, important: !oldItem.important};
			const newArr = [...data.slice(0, index), newItem, ...data.slice(index + 1)];

			return {
				data: newArr
			}
		});
	}

	onToggleLiked(id) {
		this.setState(({data}) => {
			const index = data.findIndex(element => element.id === id);
			const oldItem = data[index];
			const newItem = {...oldItem, like: !oldItem.like};
			const newArr = [...data.slice(0, index), newItem, ...data.slice(index + 1)];

			return {
				data: newArr
			}
		});
	}

	searchPosts(items, search) {
		if (search.length === 0) {
			return items;
		}
		return items.filter(item => {
			return item.label.indexOf(search) > -1;
		});
	}

	filterPosts(items, filter) {
		if (filter === 'like') {
			return items.filter(item => item.like);
		}
		else {
			return items;
		}
	}

	updateSearch(search) {
		this.setState({search});
	}

	onFilterSelect(filter) {
		this.setState({filter});
	}

	render() {
		const {data, search, filter} = this.state;

		const allItem = data.length;
		const allLiked = data.filter(item => item.like).length;

		const visiblePosts = this.filterPosts(this.searchPosts(data, search), filter);

		return (
			<AppBlock>
				<AppHeader
					allItem={allItem}
					allLiked={allLiked}
				/>
				<div className="search-panel d-flex">
					<SearchPanel
						updateSearch={this.updateSearch}
					/>
					<PostStatusFilter
						filter={filter}
						onFilterSelect={this.onFilterSelect}
					/>
				</div>
				<PostList
					posts={visiblePosts}
					onDelete={this.deleteItem}
					onToggleImportant={this.onToggleImportant}
					onToggleLiked={this.onToggleLiked}
				/>
				<PostAddForm
					onAdd={this.addItem}
				/>
			</AppBlock>
		)
	}
}